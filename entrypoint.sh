#!/bin/bash

if [[ $1 == "cantaloupe" ]] ; then
  echo "#####################################"
  echo "## Cantaloupe entrypoint           ##"
  echo "#####################################"
  echo ""
  MAX_HEAP_SIZE="${NOMAD_MEMORY_LIMIT:-2000}"
  MAX_HEAP_SIZE="$(($MAX_HEAP_SIZE / 2))m"
  MIN_HEAP_SIZE="${MAX_HEAP_SIZE}"

  MAX_MEMORY_SIZE="${NOMAD_MEMORY_LIMIT:-2000}"
  MAX_MEMORY_SIZE="${MAX_MEMORY_SIZE}m"


  if [ ! -z "$TRUSTED_CA_BUNDLE" ]; then
    echo "TRUSTED_CA_BUNDLE env var is set..."
    echo "Importing CA list into java keystore as a trusted certificate"
    echo "${TRUSTED_CA_BUNDLE}" >> /tmp/trusted_ca_bundle.crt
    keytool -import -trustcacerts -alias trusted_ca_bundle -file /tmp/trusted_ca_bundle.crt -storepass changeit -keypass changeit -noprompt -cacerts
    rm /tmp/trusted_ca_bundle.crt
  fi

  if [[ ! -f /etc/cantaloupe/cantaloupe.properties ]] ; then
    echo "No config file (/etc/cantaloupe/cantaloupe.properties) found !"
    echo "Using the sample config file as a base"
    echo "Please mount a config file to overwrite the sample."
    mkdir /etc/cantaloupe
    cp /opt/cantaloupe/cantaloupe.properties.sample /etc/cantaloupe/cantaloupe.properties
  fi

  echo "Starting Cantaloupe Java process"
  echo "#####################################"
  echo ""
  echo ""
  set -x
  exec java -Dcantaloupe.config=/etc/cantaloupe/cantaloupe.properties -XX:MaxRAM=${MAX_MEMORY_SIZE} -Xmx${MAX_HEAP_SIZE} -jar /opt/cantaloupe/cantaloupe-5.0.5.jar

else
  exec $@
fi
