# Cantaloupe IIIF Image API container

This repo contains:
- [.gitlab-ci.yml](.gitlab-ci.yml)
  A pipeline responsible for bulding tagging and pushing a custom docker image to our repo's
- [Dockerfile](Dockerfile)
  The custom docker file to build the image

The pipeline will do the following steps:

1. Build the docker image and output an artifact
2. Tag the image as latest and `${CI_PIPELINE_ID}-${CI_COMMIT_SHORT_SHA}`
3. Push the image to the nexus dev repo (automatic)
4. Push the image to the nexus uat repo (Manual step)
5. Push the image to the nexus prod repo (Manual step)

Note: In the Dockefile we use our nexus as registry to pull from.
      Nexus is configured to proxy (and thus cache ) to dockerhub
      do NOT use dockerhub directly but always point to our nexus in the `FROM` 
