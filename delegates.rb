require 'cgi'
require 'json'
require 'net/http'

class CustomDelegate

  attr_accessor :context

  def deserialize_meta_identifier(meta_identifier)
  end

  def serialize_meta_identifier(components)
  end

  def pre_authorize(options = {})
    true
  end

  def authorize(options = {})
    identifier = context['identifier']
    md5 = identifier.split('-').first
    uri = URI.parse(ENV['COLLECTION_API_URL'] + '/mediafiles/' + md5 + '/copyright')

    http = Net::HTTP.new(uri.host, uri.port)
    if uri.instance_of? URI::HTTPS
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    end

    request = Net::HTTP::Get.new(uri.request_uri)
    if context['request_headers'].has_key? "Authorization"
      # Cantaloupe sometimes strips 'Bearer' from request auth headers
      if context['request_headers']['Authorization'].start_with? "Bearer"
        request.add_field('Authorization', (context['request_headers']['Authorization']))
      else
        request.add_field('Authorization', ('Bearer ' + (context['request_headers']['Authorization'])))
      end
    elsif ENV.include? "STATIC_JWT"
      request.add_field('Authorization', ('Bearer ' + ENV['STATIC_JWT']))
    end

    response = http.request(request)
    if response.code.to_i >= 400
      puts "Request to #{uri} failed with #{response.code.to_i} #{response.body}"
      return {'status_code' => response.code.to_i}
    end

    access = JSON.parse(response.body)
    if access == 'none'
      return false
    elsif access == 'limited'
      if context['resulting_size']['height'] > 1200 or context['resulting_size']['width'] > 1200
        if context['request_uri'].include? "iiif/2"
          # v2 API does not support upscaling
          subst = 'full/!1200,1200/'
        else
          subst = 'full/^!1200,1200/'
        end
        return {
          'status_code' => 302,
          'location' => context['request_uri'].gsub(/full\/(.*?)\//, subst)
        }
      end
      return true
    else
      return true
    end
  end

  def extra_iiif2_information_response_keys(options = {})
    {}
  end

  def extra_iiif3_information_response_keys(options = {})
    {}
  end

  def source(options = {})
  end

  def azurestoragesource_blob_key(options = {})
  end

  def filesystemsource_pathname(options = {})
  end

  def httpsource_resource_info(options = {})
    identifier = context['identifier']
    ret = {'uri' => (ENV['STORAGE_API_URL'] + '/download/' + identifier)}
    if ENV.include? "STATIC_JWT"
      ret['headers'] = {'X-Custom' => 'yes', 'Authorization' => ('Bearer ' + ENV['STATIC_JWT'])}
    end
    return ret
  end

  def jdbcsource_database_identifier(options = {})
  end

  def jdbcsource_media_type(options = {})
  end

  def jdbcsource_lookup_sql(options = {})
  end

  def s3source_object_info(options = {})
  end

  def overlay(options = {})
  end

  def redactions(options = {})
    []
  end

  def metadata(options = {})
  end

end
